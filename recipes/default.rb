include_recipe 'deploy'

node[:deploy].each do |application, deploy|



bootstrap_assets_dir = deploy[:deploy_to]+deploy[:release_path]+"/protected/extensions/bootstrap/assets"

directory bootstrap_assets_dir do
  mode 0755
  owner 'root'
  group 'root'
  #recursive true
  action :create
end

end
